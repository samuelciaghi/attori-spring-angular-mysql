package com.attoriBackend.attoriBackend.Attore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttoriService {


    private final AttoriRepository attoriRepository;

    @Autowired
    public AttoriService(AttoriRepository attoriRepository) {
        this.attoriRepository = attoriRepository;
    }


    public Attore[] getAttori(){
        return  attoriRepository.caricaDatiDB();
    }
}
