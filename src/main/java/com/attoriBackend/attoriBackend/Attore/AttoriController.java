package com.attoriBackend.attoriBackend.Attore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping()
public class AttoriController {

    private final AttoriService attoriService;

    @Autowired
    public AttoriController(AttoriService attoriService) {
        this.attoriService = attoriService;
    }


    @GetMapping("/")
    public Attore[] getAttori(){
        return attoriService.getAttori();
    }

}
