package com.attoriBackend.attoriBackend.Attore;

import javax.persistence.*;

@Entity
@Table
public class Attore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;
    @Column(name = "nome")
    private String nome;
    @Column(name = "nomealternativo")
    private String nomeAlternativo;
    @Column(name= "rating")
    private int rating;

    public Attore() {

    }

    public Attore(String nome, String nomeAlternativo, int rating) {
        this.nome = nome;
        this.nomeAlternativo = nomeAlternativo;
        this.rating = rating;
    }

    public Attore(String nome, int rating) {
        this.nome = nome;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeAlternativo() {
        return nomeAlternativo;
    }

    public void setNomeAlternativo(String nomeAlternativo) {
        this.nomeAlternativo = nomeAlternativo;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Attore{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", nomeAlternativo='" + nomeAlternativo + '\'' +
                ", rating=" + rating +
                '}';
    }
}
