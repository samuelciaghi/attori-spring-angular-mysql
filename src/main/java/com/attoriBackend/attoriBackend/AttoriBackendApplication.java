package com.attoriBackend.attoriBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class AttoriBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttoriBackendApplication.class, args);
	}

}
